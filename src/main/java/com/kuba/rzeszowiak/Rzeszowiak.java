package com.kuba.rzeszowiak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by kuba3 on 25.08.2015.
 */

@EnableAutoConfiguration
@SpringBootApplication
public class Rzeszowiak {

    public static void main(String[] args)
    {
        SpringApplication.run(Rzeszowiak.class,args);

    }
}
