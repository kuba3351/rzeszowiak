package com.kuba.rzeszowiak.category;

import javax.persistence.*;

/**
 * Created by kuba3 on 27.08.2015.
 */

@Entity
@Table(name="kategoria")
public class Category {
    @Id
    @GeneratedValue(strategy= GenerationType.TABLE)
    private long id;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
