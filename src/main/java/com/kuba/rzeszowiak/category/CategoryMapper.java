package com.kuba.rzeszowiak.category;

import org.springframework.stereotype.Component;

/**
 * Created by Mateusz-Laptop on 28.08.2015.
 */
@Component
public class CategoryMapper {

    public Category mapToEntity(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setName(categoryDTO.getName());
        return category;
    }
    public Category mapToEntity(CategoryWithIdDTO categoryWithIdDTO)
    {
        CategoryDTO categoryDTO = categoryWithIdDTO;
        Category category = mapToEntity(categoryDTO);
        category.setId(categoryWithIdDTO.getid());
        return category;
    }
}
