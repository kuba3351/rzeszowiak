package com.kuba.rzeszowiak.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Locale;

/**
 * Created by kuba3 on 27.08.2015.
 */
@RepositoryRestResource
public interface CategoryRepository  extends JpaRepository<Category,Long> {

}
