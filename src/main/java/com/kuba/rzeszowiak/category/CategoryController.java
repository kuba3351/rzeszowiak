package com.kuba.rzeszowiak.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by kuba3 on 27.08.2015.
 */

@RestController
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CategoryMapper categoryMapper;

    @RequestMapping(value = "/api/category/add", method = RequestMethod.POST)
    public void saveCategory(@RequestBody @Valid CategoryDTO category)
    {
        categoryRepository.save(categoryMapper.mapToEntity(category));
    }
    @RequestMapping(value="/api/category/display")
    public List<Category> display()
    {
        List<Category> users = categoryRepository.findAll();
        return users;
    }
    @RequestMapping(value="/api/category/remove",method=RequestMethod.GET)
    @ResponseBody
    public String remove(@RequestParam(value="id") long id)
    {
        if(!categoryRepository.exists(id))
            return "Error: There is no category with given id";
        else
        {
            categoryRepository.delete(id);
            return "Operation was successfull";
        }
    }
    @RequestMapping(value="/api/category/rename",method=RequestMethod.POST)
    public void rename(@RequestBody @Valid CategoryWithIdDTO category)
    {
        if(!categoryRepository.exists(category.getid()))
            return;
        categoryRepository.save(categoryMapper.mapToEntity(category));
    }
}
